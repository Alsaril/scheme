#pragma once

#include <memory>
#include <error.h>
#include <runtime.h>
#include <scope.h>

class RuntimeCallable {
public:
    virtual ~RuntimeCallable() = default;
    virtual std::shared_ptr<RuntimeObject> Eval(std::shared_ptr<Scope> scope) = 0;
};

class StaticRuntimeCallable : public RuntimeCallable {
public:
    StaticRuntimeCallable(std::shared_ptr<RuntimeObject> value) : value_(value) {
    }

    virtual std::shared_ptr<RuntimeObject> Eval(std::shared_ptr<Scope>) override {
        return value_;
    }

private:
    std::shared_ptr<RuntimeObject> value_;
};

template <class L>
class LambdaRuntimeCallable : public RuntimeCallable {
public:
    LambdaRuntimeCallable(L lambda) : lambda_(lambda) {
    }

    virtual std::shared_ptr<RuntimeObject> Eval(std::shared_ptr<Scope> scope) override {
        return lambda_(scope);
    }

private:
    L lambda_;
};
