#pragma once

#include <variant>
#include <optional>
#include <istream>
#include <error.h>

struct SymbolToken {
    std::string name;

    SymbolToken(const std::string& name) : name(name) {
    }

    bool operator==(const SymbolToken& other) const;
};

struct QuoteToken {
    bool operator==(const QuoteToken&) const;
};

struct DotToken {
    bool operator==(const DotToken&) const;
};

enum class BracketToken { OPEN, CLOSE };

struct ConstantToken {
    int value;

    ConstantToken(int value) : value(value) {
    }

    bool operator==(const ConstantToken& other) const;
};

typedef std::variant<ConstantToken, BracketToken, SymbolToken, QuoteToken, DotToken> Token;

// Интерфейс позволяющий читать токены по одному из потока.
class Tokenizer {
public:
    Tokenizer(std::istream* in);
    bool IsEnd();
    void Next();
    Token& GetToken();

private:
    bool IsDigit(char c) {
        return (c >= '0') && (c <= '9');
    }

    bool IsSymbolStart(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c == '<') || (c == '=') ||
               (c == '>') || (c == '*') || (c == '#');
    }

    bool IsSymbol(char c) {
        return IsSymbolStart(c) || IsDigit(c) || (c == '?') || (c == '!') || (c == '-');
    }

    int ReadInt(char c) {
        int number = c - '0';
        while (true) {
            int next = in_.peek();
            if (next == EOF || !IsDigit(next)) {
                return number;
            }
            in_.get(c);
            number *= 10;
            number += c - '0';
        }
    }

    std::istream& in_;
    bool end_ = false;
    Token current_;
};