#pragma once

#include <unordered_map>
#include <runtime.h>

class Scope {
public:
    Scope() = default;

    Scope(std::initializer_list<std::shared_ptr<Scope>> parents) : parents_(parents) {
    }

    Scope(const Scope&) = delete;
    Scope& operator=(const Scope&) = delete;
    Scope(Scope&& other) = delete;
    Scope& operator=(Scope&& other) = delete;

    void Put(const std::string& name, std::shared_ptr<RuntimeObject> obj) {
        storage_[name] = obj;
    }

    void Update(const std::string& name, std::shared_ptr<RuntimeObject> obj) {
        if (storage_.count(name) > 0) {
            storage_[name] = obj;
            return;
        }
        for (auto parent : parents_) {
            if (parent->Contains(name)) {
                parent->Update(name, obj);
                return;
            }
        }
    }

    std::shared_ptr<RuntimeObject> Get(const std::string& name) const {
        if (storage_.count(name) > 0) {
            return storage_[name];
        }
        for (auto parent : parents_) {
            if (parent->Contains(name)) {
                return parent->Get(name);
            }
        }
        return {};
    }

    bool Contains(const std::string& name) const {
        if (storage_.count(name) > 0) {
            return true;
        }
        for (auto parent : parents_) {
            if (parent->Contains(name)) {
                return true;
            }
        }
        return false;
    }

    void Clear() {
        storage_.clear();
    }

private:
    std::vector<std::shared_ptr<Scope>> parents_;
    mutable std::unordered_map<std::string, std::shared_ptr<RuntimeObject>> storage_;
};