#pragma once

#include <memory>
#include <callable.h>
#include <object.h>
#include <scheme.h>
#include <scope.h>
#include <iostream>

class ScopeOperation {
public:
    virtual ~ScopeOperation() = default;
    virtual std::shared_ptr<RuntimeCallable> Exec(Interpreter& interpreter,
                                                  std::shared_ptr<Cell> cell) = 0;

protected:
    std::shared_ptr<RuntimeCallable> EvalInternal(Interpreter& interpreter,
                                                  std::shared_ptr<Object> obj,
                                                  bool quoted = false) {
        return interpreter.BuildAST(obj, obj->Quoted() || quoted);
    }

    std::vector<std::string> ExtractList(std::shared_ptr<Cell> cell, Interpreter& interpreter,
                                         std::shared_ptr<Scope> scope) {
        auto args = cell ? As<List>(EvalInternal(interpreter, cell, true)->Eval(scope))
                         : std::make_shared<List>(true);

        std::vector<std::string> names;
        for (size_t i = 0; i < args->Size(); ++i) {
            names.push_back(As<String>(args->At(i))->ToString());
        }

        return names;
    }

    std::vector<std::shared_ptr<RuntimeCallable>> ExtractStatements(std::shared_ptr<Cell> cell,
                                                                    Interpreter& interpreter) {
        std::vector<std::shared_ptr<RuntimeCallable>> statements;

        while (cell) {
            statements.push_back(EvalInternal(interpreter, cell->GetFirst()));
            if (!cell->GetSecond()) {
                break;
            }
            cell = As<Cell>(cell->GetSecond());
        }

        return statements;
    }
};

const std::shared_ptr<ScopeOperation> ParseScopeOperator(const std::string& symbol);

class DefineOperation : public ScopeOperation {
public:
    virtual std::shared_ptr<RuntimeCallable> Exec(Interpreter& interpreter,
                                                  std::shared_ptr<Cell> cell) override {
        auto lambda = [cell, &interpreter,
                       this](std::shared_ptr<Scope> scope) -> std::shared_ptr<RuntimeObject> {
            if (cell->Size() != 2) {
                throw SyntaxError{"bad define syntax"};
            }

            if (Is<Symbol>(cell->GetFirst())) {
                std::string name = As<Symbol>(cell->GetFirst())->GetName();
                scope->Put(name, std::make_shared<PrototypeOperator>());
                std::shared_ptr<RuntimeCallable> value =
                    EvalInternal(interpreter, As<Cell>(cell->GetSecond())->GetFirst());

                auto result = value->Eval(scope);
                if (Is<LambdaOperator>(result)) {
                    As<PrototypeOperator>(scope->Get(name))->Set(As<LambdaOperator>(result));
                } else {
                    scope->Put(name, result);
                }
                return {};
            } else if (Is<Cell>(cell->GetFirst())) {
                auto names = ExtractList(As<Cell>(cell->GetFirst()), interpreter, scope);
                auto statements = ExtractStatements(As<Cell>(cell->GetSecond()), interpreter);
                std::string name = names.front();
                names.erase(names.begin());

                scope->Put(name, std::make_shared<LambdaOperator>(std::move(names), scope,
                                                                  std::move(statements)));
                return {};
            }

            throw SyntaxError{"bad define syntax"};
        };

        return std::make_shared<LambdaRuntimeCallable<decltype(lambda)>>(lambda);
    }
};

class SetOperation : public ScopeOperation {
public:
    virtual std::shared_ptr<RuntimeCallable> Exec(Interpreter& interpreter,
                                                  std::shared_ptr<Cell> cell) override {
        auto lambda = [cell, &interpreter,
                       this](std::shared_ptr<Scope> scope) -> std::shared_ptr<RuntimeObject> {
            if (cell->Size() != 2) {
                throw SyntaxError{"bad set syntax"};
            }
            std::string name = As<Symbol>(cell->GetFirst())->GetName();
            std::shared_ptr<RuntimeCallable> value =
                EvalInternal(interpreter, As<Cell>(cell->GetSecond())->GetFirst());

            if (!scope->Contains(name)) {
                throw NameError{"unknown name: " + name};
            }
            scope->Update(name, value->Eval(scope));
            return {};
        };

        return std::make_shared<LambdaRuntimeCallable<decltype(lambda)>>(lambda);
    }
};

class SetPairOperation : public ScopeOperation {
public:
    SetPairOperation(bool first) : first_(first) {
    }

    virtual std::shared_ptr<RuntimeCallable> Exec(Interpreter& interpreter,
                                                  std::shared_ptr<Cell> cell) override {
        bool first = first_;

        auto lambda = [first, cell, &interpreter,
                       this](std::shared_ptr<Scope> scope) -> std::shared_ptr<RuntimeObject> {
            std::string name = As<Symbol>(cell->GetFirst())->GetName();
            std::shared_ptr<RuntimeCallable> value =
                EvalInternal(interpreter, As<Cell>(cell->GetSecond())->GetFirst());

            if (!scope->Contains(name)) {
                throw NameError{"unknown name: " + name};
            }
            auto list = As<List>(scope->Get(name));
            if (!list->IsPair()) {
                throw RuntimeError{"pair expected"};
            }
            list->At(first ? 0 : 1) = value->Eval(scope);
            return {};
        };

        return std::make_shared<LambdaRuntimeCallable<decltype(lambda)>>(lambda);
    }

private:
    bool first_;
};

class LambdaOperation : public ScopeOperation {
public:
    virtual std::shared_ptr<RuntimeCallable> Exec(Interpreter& interpreter,
                                                  std::shared_ptr<Cell> cell) override {
        auto lambda = [cell, &interpreter,
                       this](std::shared_ptr<Scope> scope) -> std::shared_ptr<RuntimeObject> {
            if (cell->Size() < 2) {
                throw SyntaxError{"bad lambda syntax"};
            }

            auto names = ExtractList(cell->GetFirst() ? As<Cell>(cell->GetFirst()) : nullptr,
                                     interpreter, scope);
            auto statements = ExtractStatements(As<Cell>(cell->GetSecond()), interpreter);

            return std::make_shared<LambdaOperator>(std::move(names), scope, std::move(statements));
        };

        return std::make_shared<LambdaRuntimeCallable<decltype(lambda)>>(lambda);
    }
};