#include <parser.h>

bool ValidToken(std::shared_ptr<Object> obj) {
    return !Is<OpenBracket>(obj) && !Is<Dot>(obj);
}

void BuildList(std::vector<std::shared_ptr<Object>>& symbols, bool expect_bracket) {
    if (symbols.empty()) {
        throw SyntaxError{"not enough arguments"};
    }
    std::shared_ptr<Object> curr = symbols.back();
    symbols.pop_back();
    while (!symbols.empty() && !Is<OpenBracket>(symbols.back())) {
        if (symbols.size() > 1 && Is<Dot>(symbols[symbols.size() - 2])) {
            if (curr.get() != nullptr) {
                throw SyntaxError{"bad dot syntax"};
            }
            curr = symbols.back();  // throw curr as it is just nullptr
            symbols.pop_back();
            symbols.pop_back();  // throw dot
        }
        if (symbols.empty()) {
            throw SyntaxError{"not enough arguments"};
        }
        std::shared_ptr<Object> right = symbols.back();
        symbols.pop_back();

        if (Is<Quote>(right)) {
            if (Is<Cell>(curr)) {
                As<Cell>(curr)->GetFirst()->Quote();
            } else {
                curr->Quote();
            }
            continue;
        }

        if (!ValidToken(right) || !ValidToken(curr)) {
            throw SyntaxError{"ill formed expression"};
        }

        curr = std::make_shared<Cell>(right, curr);
    }

    if (expect_bracket) {
        if (symbols.empty() || !Is<OpenBracket>(symbols.back())) {
            throw SyntaxError{"open bracket expected"};
        } else {
            symbols.pop_back();
        }
    }

    if (!symbols.empty() && Is<Quote>(symbols.back())) {
        if (curr) {
            curr->Quote();
        } else {
            curr = std::make_shared<QuotedNullptr>();
        }
        symbols.pop_back();
    }

    symbols.push_back(curr);
}

std::shared_ptr<Object> Read(Tokenizer* tokenizer) {
    std::vector<std::shared_ptr<Object>> symbols;

    while (!tokenizer->IsEnd()) {
        auto& token = tokenizer->GetToken();
        if (ConstantToken* num = std::get_if<ConstantToken>(&token)) {
            symbols.push_back(std::make_shared<Number>(num->value));
        } else if (BracketToken* bracket = std::get_if<BracketToken>(&token)) {
            if (*bracket == BracketToken::OPEN) {
                symbols.push_back(std::make_shared<OpenBracket>());
            } else {
                symbols.push_back(std::shared_ptr<Object>());
                BuildList(symbols, true);
            }
        } else if (SymbolToken* symbol = std::get_if<SymbolToken>(&token)) {
            symbols.push_back(std::make_shared<Symbol>(symbol->name));
        } else if (std::get_if<QuoteToken>(&token)) {
            symbols.push_back(std::make_shared<Quote>());
        } else if (std::get_if<DotToken>(&token)) {
            symbols.push_back(std::make_shared<Dot>());
        } else {
            throw SyntaxError{"unknown token"};
        }

        tokenizer->Next();
    }

    BuildList(symbols, false);

    if (symbols.size() != 1 || !ValidToken(symbols.back())) {
        throw SyntaxError{"ill formed expression"};
    }

    return symbols.back();
}
