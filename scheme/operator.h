#pragma once

#include <string>
#include <optional>
#include <functional>
#include <error.h>
#include <runtime.h>
#include <callable.h>
#include <scope.h>

class Operator {
public:
    virtual ~Operator() = default;
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>& args,
        std::shared_ptr<Scope>) const = 0;
};

const std::shared_ptr<Operator> ParseOperator(const std::string& symbol);

template <class A, class B>
class UnaryOperator : public Operator {
protected:
    using Ret = std::shared_ptr<B>;
    virtual Ret Internal(const A*) const = 0;

public:
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>& args,
        std::shared_ptr<Scope> scope) const final override {
        if (args.size() != 1) {
            throw RuntimeError{"wrong number of arguments"};
        }
        auto arg = As<A>(args.front()->Eval(scope));
        return Internal(arg.get());
    }
};

class Predicate : public UnaryOperator<RuntimeObject, Boolean> {
protected:
    virtual bool Test(const RuntimeObject*) const = 0;
    virtual Ret Internal(const RuntimeObject*) const final override;
};

template <class T>
class TestType : public Predicate {
public:
    TestType() = default;

    TestType(std::function<bool(const T&)> lambda) : lambda_(lambda) {
    }

protected:
    virtual bool Test(const RuntimeObject* obj) const override {
        const T* t = dynamic_cast<const T*>(obj);
        return t && (!lambda_ || (*lambda_)(*t));
    }

private:
    std::optional<std::function<bool(const T&)>> lambda_;
};

class NotOperator : public UnaryOperator<RuntimeObject, Boolean> {
protected:
    virtual Ret Internal(const RuntimeObject*) const override;
};

class AbsOperator : public UnaryOperator<Int, Int> {
protected:
    virtual Ret Internal(const Int*) const override;
};

class BooleanOperator : public Operator {
public:
    BooleanOperator(bool def) : default_(Boolean::ValueOf(def)) {
    }

    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;

private:
    std::shared_ptr<Boolean> default_;
};

class CompareOperator : public Operator {
public:
    enum Type { LE, LQ, EQ, GQ, GR };

    CompareOperator(Type type) : type_(type) {
    }

    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const override;

private:
    Type type_;

    bool Test(int64_t left, int64_t right) const;
};

class IntegerOperator : public Operator {
public:
    IntegerOperator(bool comm, int64_t id, std::function<int64_t(int64_t, int64_t)> op)
        : comm_(comm), id_(id), op_(op) {
    }

    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;

private:
    bool comm_;
    int64_t id_;
    std::function<int64_t(int64_t, int64_t)> op_;
};

class IfOperator : public Operator {
public:
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;
};

class ListOperator : public Operator {
public:
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;
};

class ConsOperator : public Operator {
public:
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;
};

class PairIndexOperator : public UnaryOperator<List, RuntimeObject> {
public:
    PairIndexOperator(size_t index) : index_(index) {
    }

protected:
    virtual Ret Internal(const List*) const override;

private:
    size_t index_;
};

class ListIndexOperator : public Operator {
public:
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;
};

class ListTailOperator : public Operator {
public:
    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;
};

class LambdaOperator : public Operator, public RuntimeObject {
public:
    LambdaOperator(std::vector<std::string>&& names, std::shared_ptr<Scope> parent,
                   std::vector<std::shared_ptr<RuntimeCallable>>&& statements)
        : names_(std::move(names)), parent_(parent), statements_(std::move(statements)) {
    }

    virtual std::shared_ptr<RuntimeObject> Invoke(
        const std::vector<std::shared_ptr<RuntimeCallable>>&,
        std::shared_ptr<Scope>) const final override;

    virtual std::string ToString() const override {
        return "lambda";
    }

private:
    std::vector<std::string> names_;
    std::shared_ptr<Scope> parent_;
    std::vector<std::shared_ptr<RuntimeCallable>> statements_;
};

class PrototypeOperator : public Operator, public RuntimeObject {
public:
    void Set(std::shared_ptr<LambdaOperator> real) {
        real_ = real;
    }

    std::shared_ptr<RuntimeObject> Invoke(const std::vector<std::shared_ptr<RuntimeCallable>>& args,
                                          std::shared_ptr<Scope> scope) const final override {
        return real_->Invoke(args, scope);
    }

    std::string ToString() const override {
        return real_->ToString();
    }

private:
    std::shared_ptr<LambdaOperator> real_;
};
