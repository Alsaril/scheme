#include <object.h>

int Number::GetValue() const {
    return value_;
}

const std::string& Symbol::GetName() const {
    return name_;
}

std::shared_ptr<Object> Cell::GetFirst() const {
    return first_;
}

std::shared_ptr<Object> Cell::GetSecond() const {
    return second_;
}

template <>
std::shared_ptr<Object> Get<0>(std::shared_ptr<Cell> obj) {
    return obj->GetFirst();
}

template <>
void ExpectListOfSize<0>(std::shared_ptr<Object> obj) {
    if (obj.get() != nullptr) {
        throw RuntimeError{"too long list"};
    }
}

std::optional<size_t> Cell::Size() const noexcept {
    size_t result = 1;
    std::shared_ptr<Object> curr = second_;
    while (curr) {
        result++;
        if (Is<Cell>(curr)) {
            curr = As<Cell>(curr)->GetSecond();
        } else {
            return {};
        }
    }
    return result;
}