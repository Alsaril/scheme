#pragma once

#include <catch.hpp>
#include <error.h>

#include <scheme.h>

struct SchemeTest {
    Interpreter interpreter;

    SchemeTest() = default;

    // Implement following methods.
    void ExpectEq(const std::string& expression, const std::string& result) {
        REQUIRE(interpreter.Eval(expression) == result);
    }

    void ExpectNoError(const std::string& expression) {
        REQUIRE_NOTHROW(interpreter.Eval(expression));
    }

    void ExpectSyntaxError(const std::string& expression) {
        REQUIRE_THROWS_AS(interpreter.Eval(expression), SyntaxError);
    }

    void ExpectRuntimeError(const std::string& expression) {
        REQUIRE_THROWS_AS(interpreter.Eval(expression), RuntimeError);
    }

    void ExpectNameError(const std::string& expression) {
        REQUIRE_THROWS_AS(interpreter.Eval(expression), NameError);
    }
};
