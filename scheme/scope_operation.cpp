#include <scope_operation.h>

const std::shared_ptr<ScopeOperation> ParseScopeOperator(const std::string& symbol) {
    if (symbol == "define") {
        return std::make_shared<DefineOperation>();
    } else if (symbol == "set!") {
        return std::make_shared<SetOperation>();
    } else if (symbol == "set-car!") {
        return std::make_shared<SetPairOperation>(true);
    } else if (symbol == "set-cdr!") {
        return std::make_shared<SetPairOperation>(false);
    } else if (symbol == "lambda") {
        return std::make_shared<LambdaOperation>();
    }

    return {};
}