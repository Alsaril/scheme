#include <operator.h>
#include <unordered_map>

using Oper = std::shared_ptr<Operator>;

const static std::unordered_map<std::string, Oper> kOperators{
    {"boolean?", std::make_shared<TestType<Boolean>>()},
    {"number?", std::make_shared<TestType<Int>>()},
    {"symbol?", std::make_shared<TestType<String>>()},
    {"pair?", std::make_shared<TestType<List>>([](const List& list) { return list.Size() > 0; })},
    {"null?", std::make_shared<TestType<List>>([](const List& list) { return list.Size() == 0; })},
    {"list?", std::make_shared<TestType<List>>([](const List& list) { return list.Proper(); })},
    {"not", std::make_shared<NotOperator>()},
    {"<", std::make_shared<CompareOperator>(CompareOperator::LE)},
    {"<=", std::make_shared<CompareOperator>(CompareOperator::LQ)},
    {"=", std::make_shared<CompareOperator>(CompareOperator::EQ)},
    {">=", std::make_shared<CompareOperator>(CompareOperator::GQ)},
    {">", std::make_shared<CompareOperator>(CompareOperator::GR)},
    {"and", std::make_shared<BooleanOperator>(true)},
    {"or", std::make_shared<BooleanOperator>(false)},
    {"+", std::make_shared<IntegerOperator>(true, 0, [](int64_t a, int64_t b) { return a + b; })},
    {"-", std::make_shared<IntegerOperator>(false, 0, [](int64_t a, int64_t b) { return a - b; })},
    {"*", std::make_shared<IntegerOperator>(true, 1, [](int64_t a, int64_t b) { return a * b; })},
    {"/", std::make_shared<IntegerOperator>(false, 1, [](int64_t a, int64_t b) { return a / b; })},
    {"max", std::make_shared<IntegerOperator>(false, 1,
                                              [](int64_t a, int64_t b) { return std::max(a, b); })},
    {"min", std::make_shared<IntegerOperator>(false, 1,
                                              [](int64_t a, int64_t b) { return std::min(a, b); })},
    {"abs", std::make_shared<AbsOperator>()},
    {"if", std::make_shared<IfOperator>()},
    {"list", std::make_shared<ListOperator>()},
    {"cons", std::make_shared<ConsOperator>()},
    {"car", std::make_shared<PairIndexOperator>(0)},
    {"cdr", std::make_shared<PairIndexOperator>(1)},
    {"list-ref", std::make_shared<ListIndexOperator>()},
    {"list-tail", std::make_shared<ListTailOperator>()}};

const Oper ParseOperator(const std::string& symbol) {
    auto it = kOperators.find(symbol);
    return it == kOperators.end() ? Oper() : it->second;
}

Predicate::Ret Predicate::Internal(const RuntimeObject* obj) const {
    return Boolean::ValueOf(Test(obj));
}

NotOperator::Ret NotOperator::Internal(const RuntimeObject* obj) const {
    return Boolean::ValueOf(!obj->ToBool());
}

AbsOperator::Ret AbsOperator::Internal(const Int* arg) const {
    return std::make_shared<Int>(std::abs(arg->Value()));
}

std::shared_ptr<RuntimeObject> BooleanOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.empty()) {
        return default_;
    }

    for (size_t i = 0; i + 1 < args.size(); ++i) {
        auto arg = args[i]->Eval(scope);
        if (arg->ToBool() != default_->ToBool()) {
            return arg;
        }
    }

    return args.back()->Eval(scope);
}

std::shared_ptr<RuntimeObject> CompareOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.size() < 2) {
        return Boolean::kTrue;
    }
    std::shared_ptr<Int> current = As<Int>(args.front()->Eval(scope));
    for (size_t i = 1; i < args.size(); ++i) {
        auto next = As<Int>(args[i]->Eval(scope));
        if (!Test(current->Value(), next->Value())) {
            return Boolean::kFalse;
        }
        current = next;
    }
    return Boolean::kTrue;
}

bool CompareOperator::Test(int64_t left, int64_t right) const {
    switch (type_) {
        case Type::LE:
            return left < right;
        case Type::LQ:
            return left <= right;
        case Type::EQ:
            return left == right;
        case Type::GQ:
            return left >= right;
        case Type::GR:
            return left > right;
    }
    throw RuntimeError{"unknown comparison"};
}

std::shared_ptr<RuntimeObject> IntegerOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    int64_t acc = id_;
    if (!comm_) {
        if (args.empty()) {
            throw RuntimeError{"at least one argument required"};
        }
        acc = As<Int>(args.front()->Eval(scope))->Value();
    }

    for (size_t i = comm_ ? 0 : 1; i < args.size(); ++i) {
        acc = op_(acc, As<Int>(args[i]->Eval(scope))->Value());
    }
    return std::make_shared<Int>(acc);
}

std::shared_ptr<RuntimeObject> IfOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.size() < 2 || args.size() > 3) {
        throw SyntaxError{"bad if syntax"};
    }
    bool cond = As<Boolean>(args[0]->Eval(scope))->ToBool();
    std::shared_ptr<RuntimeCallable> second =
        std::make_shared<StaticRuntimeCallable>(std::make_shared<List>(true));
    if (args.size() == 3) {
        second = args[2];
    }

    if (cond) {
        return args[1]->Eval(scope);
    } else {
        return second->Eval(scope);
    }
}

std::shared_ptr<RuntimeObject> ListOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    std::vector<std::shared_ptr<RuntimeObject>> evaluated_args;
    for (auto& callable : args) {
        evaluated_args.push_back(callable->Eval(scope));
    }
    return std::make_shared<List>(true, std::move(evaluated_args));
}

std::shared_ptr<RuntimeObject> ConsOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.size() != 2) {
        throw RuntimeError{"wrong number of arguments"};
    }
    std::vector<std::shared_ptr<RuntimeObject>> evaluated_args;
    for (auto& callable : args) {
        evaluated_args.push_back(callable->Eval(scope));
    }
    return std::make_shared<List>(false, std::move(evaluated_args));
}

PairIndexOperator::Ret PairIndexOperator::Internal(const List* pair) const {
    if (!pair->IsPair()) {
        throw SyntaxError{"bad argument"};
    }
    return pair->At(index_);
}

std::shared_ptr<RuntimeObject> ListIndexOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.size() != 2) {
        throw RuntimeError{"2 arguments are expected"};
    }
    auto list = As<List>(args.front()->Eval(scope));
    auto index = As<Int>(args[1]->Eval(scope))->Value();
    if (index >= static_cast<int64_t>(list->Size())) {
        throw RuntimeError{"bad index"};
    }
    return list->At(index);
}

std::shared_ptr<RuntimeObject> ListTailOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.size() != 2) {
        throw RuntimeError{"2 arguments are expected"};
    }
    auto list = As<List>(args.front()->Eval(scope));
    auto index = As<Int>(args[1]->Eval(scope))->Value();
    if (index > static_cast<int64_t>(list->Size())) {
        throw RuntimeError{"bad index"};
    }

    std::vector<std::shared_ptr<RuntimeObject>> result;
    while (index < static_cast<int64_t>(list->Size())) {
        result.push_back(list->At(index));
        ++index;
    }

    return std::make_shared<List>(true, std::move(result));
}

std::shared_ptr<RuntimeObject> LambdaOperator::Invoke(
    const std::vector<std::shared_ptr<RuntimeCallable>>& args, std::shared_ptr<Scope> scope) const {
    if (args.size() != names_.size()) {
        throw RuntimeError{"wrong number of arguments"};
    }

    // make copy of scope, eval on it
    auto local =
        std::make_shared<Scope>(std::initializer_list<std::shared_ptr<Scope>>{parent_, scope});

    for (size_t i = 0; i < names_.size(); ++i) {  // fill scope
        local->Put(names_[i], args[i]->Eval(local));
    }

    std::shared_ptr<RuntimeObject> result;
    for (auto statement : statements_) {
        result = statement->Eval(local);
    }

    return result;
}
