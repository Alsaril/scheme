#pragma once

#include <string>
#include <error.h>
#include <optional>
#include <memory>

class Object {
public:
    virtual ~Object() = default;

    void Quote() {
        quoted_ = true;
    }

    bool Quoted() {
        return quoted_;
    }

private:
    bool quoted_ = false;
};

class OpenBracket : public Object {};

class Dot : public Object {};

class Quote : public Object {};

class Number : public Object {
public:
    Number(int value) : value_(value) {
    }

    int GetValue() const;

private:
    int value_;
};

class Symbol : public Object {
public:
    Symbol(const std::string& name) : name_(name) {
    }

    const std::string& GetName() const;

private:
    std::string name_;
};

class Cell : public Object {
public:
    Cell(std::shared_ptr<Object> first, std::shared_ptr<Object> second)
        : first_(first), second_(second) {
    }

    std::shared_ptr<Object> GetFirst() const;
    std::shared_ptr<Object> GetSecond() const;

    std::optional<size_t> Size() const noexcept;

private:
    std::shared_ptr<Object> first_;
    std::shared_ptr<Object> second_;
};

class QuotedNullptr : public Object {};

template <class T>
std::shared_ptr<T> As(const std::shared_ptr<Object>& obj) {
    auto result = std::dynamic_pointer_cast<T>(obj);
    if (!result) {
        throw RuntimeError{"bad type"};
    }
    return result;
}

template <class T>
bool Is(const std::shared_ptr<Object>& obj) {
    return dynamic_cast<T*>(obj.get()) != nullptr;
}

template <size_t index>
std::shared_ptr<Object> Get(std::shared_ptr<Cell> obj) {
    return Get<index - 1>(As<Cell>(obj->GetSecond()));
}

template <>
std::shared_ptr<Object> Get<0>(std::shared_ptr<Cell> obj);

template <size_t len>
void ExpectListOfSize(std::shared_ptr<Object> obj) {
    ExpectListOfSize<len - 1>(As<Cell>(obj)->GetSecond());
}

template <>
void ExpectListOfSize<0>(std::shared_ptr<Object> obj);