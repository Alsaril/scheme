#include <tokenizer.h>

bool SymbolToken::operator==(const SymbolToken& other) const {
    return name == other.name;
}

bool QuoteToken::operator==(const QuoteToken&) const {
    return true;
}

bool DotToken::operator==(const DotToken&) const {
    return true;
}

bool ConstantToken::operator==(const ConstantToken& other) const {
    return value == other.value;
}

Tokenizer::Tokenizer(std::istream* in) : in_(*in), current_(QuoteToken()) {
    Next();
}

bool Tokenizer::IsEnd() {
    return end_;
}

void Tokenizer::Next() {
    if (!in_) {
        end_ = true;
        return;
    }

    char c;

    // consume spaces
    do {
        in_.get(c);
    } while (in_ && (c == ' ' || c == '\n'));

    if (!in_) {
        end_ = true;
        return;
    }

    end_ = false;

    // consume digits, return
    if (IsDigit(c)) {
        current_ = ConstantToken(ReadInt(c));
        return;
    }

    // try to parse signed int
    if (c == '+' || c == '-') {
        bool sign = c == '-';
        int next = in_.peek();
        if (next != EOF && IsDigit(next)) {
            in_.get(c);
            int value = ReadInt(c);
            if (sign) {
                value = -value;
            }
            current_ = ConstantToken(value);
            return;
        }
    }

    // consume text, return
    if (IsSymbolStart(c)) {
        std::string symbol;
        symbol += c;
        while (true) {
            int next = in_.peek();
            if (next == EOF || !IsSymbol(next)) {
                current_ = SymbolToken(symbol);
                return;
            }
            in_.get(c);
            symbol += c;
        }
    }

    // consume symbol, return
    switch (c) {
        case '+':
            current_ = SymbolToken("+");
            return;
        case '-':
            current_ = SymbolToken("-");
            return;
        case '/':
            current_ = SymbolToken("/");
            return;
        case '(':
            current_ = BracketToken::OPEN;
            return;
        case ')':
            current_ = BracketToken::CLOSE;
            return;
        case '\'':
            current_ = QuoteToken();
            return;
        case '.':
            current_ = DotToken();
            return;
    }

    throw SyntaxError{std::string("unknown token: ") + c};
}

Token& Tokenizer::GetToken() {
    return current_;
}