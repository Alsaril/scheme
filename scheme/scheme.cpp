#include <scheme.h>

#include <scope_operation.h>

std::string Interpreter::Eval(const std::string& expression) {
    std::stringstream ss{expression};
    Tokenizer tokenizer{&ss};
    std::shared_ptr<Object> obj = Read(&tokenizer);
    if (!obj) {
        throw RuntimeError{"evaluating null"};
    }
    std::shared_ptr<RuntimeObject> result = BuildAST(obj, obj->Quoted())->Eval(scope_);
    return result ? result->ToString() : "";
}

// virtual
std::shared_ptr<RuntimeCallable> Interpreter::BuildAST(std::shared_ptr<Object> obj, bool quoted) {
    auto lambda = [obj, quoted,
                   this](std::shared_ptr<Scope> scope) -> std::shared_ptr<RuntimeObject> {
        if (obj.get() == nullptr || Is<QuotedNullptr>(obj)) {
            return std::make_shared<List>(true);
        } else if (Is<Number>(obj)) {
            return std::make_shared<Int>(As<Number>(obj)->GetValue());
        } else if (Is<Symbol>(obj)) {
            return EvalSymbol(As<Symbol>(obj)->GetName(), quoted, scope);
        } else if (Is<Cell>(obj)) {
            return EvalCell(As<Cell>(obj), quoted, scope);
        }
        throw RuntimeError{"wtf unknown object"};
    };
    return std::make_shared<LambdaRuntimeCallable<decltype(lambda)>>(lambda);
}

// real
std::shared_ptr<RuntimeObject> Interpreter::EvalSymbol(const std::string& name, bool quoted,
                                                       std::shared_ptr<Scope> scope) {
    if (quoted) {
        return std::make_shared<String>(name);
    } else if (name == "#t") {
        return Boolean::kTrue;
    } else if (name == "#f") {
        return Boolean::kFalse;
    } else if (scope->Contains(name)) {
        return scope->Get(name);
    }
    throw NameError{"undefined name: " + name};
}

// real
std::pair<std::vector<std::shared_ptr<RuntimeCallable>>, bool> Interpreter::BuildList(
    std::shared_ptr<Cell> cell, bool quoted) {
    std::vector<std::shared_ptr<RuntimeCallable>> callable_args;

    bool proper = true;
    while (cell.get() != nullptr) {
        auto first = cell->GetFirst();
        callable_args.push_back(BuildAST(first, quoted || (first && first->Quoted())));
        auto next = cell->GetSecond();
        if (!next) {
            break;
        }
        if (!Is<Cell>(next)) {
            proper = false;
            callable_args.push_back(BuildAST(next, quoted || next->Quoted()));
            break;
        }
        cell = As<Cell>(next);
    }

    return std::make_pair(callable_args, proper);
}

// real
std::shared_ptr<RuntimeObject> CopyList(const std::vector<std::shared_ptr<RuntimeCallable>>& list,
                                        bool proper, std::shared_ptr<Scope> scope) {
    std::vector<std::shared_ptr<RuntimeObject>> evaluated_args;
    for (auto& callable : list) {
        evaluated_args.push_back(callable->Eval(scope));
    }
    return std::make_shared<List>(proper, std::move(evaluated_args));
}

// real
std::shared_ptr<RuntimeObject> Interpreter::EvalCell(std::shared_ptr<Cell> cell, bool quoted,
                                                     std::shared_ptr<Scope> scope) {
    if (quoted) {
        auto [list, proper] = BuildList(cell, quoted);
        return CopyList(list, proper, scope);
    }

    std::shared_ptr<Operator> op;

    if (Is<Cell>(cell->GetFirst())) {
        auto inner = As<Cell>(cell->GetFirst());
        op = As<LambdaOperator>(EvalCell(inner, inner->Quoted(), scope));
    } else {
        if (!Is<Symbol>(cell->GetFirst())) {
            throw RuntimeError{"symbol expected"};
        }

        std::string symbol = As<Symbol>(cell->GetFirst())->GetName();

        if (symbol == "quote") {
            return BuildAST(Get<1>(cell), true)->Eval(scope);
        }

        if (auto sop = ParseScopeOperator(symbol); sop) {
            if (!Is<Cell>(cell->GetSecond())) {
                throw SyntaxError{"list expected"};
            }
            return sop->Exec(*this, As<Cell>(cell->GetSecond()))->Eval(scope);
        }

        op = ParseOperator(symbol);

        if (!op && scope->Contains(symbol)) {
            auto candidate = scope->Get(symbol);
            if (Is<LambdaOperator>(candidate)) {
                op = As<LambdaOperator>(candidate);
            } else if (Is<PrototypeOperator>(candidate)) {
                op = As<PrototypeOperator>(candidate);
            }
        }

        if (!op) {
            throw NameError{"unknown name: " + symbol};
        }
    }

    auto next = cell->GetSecond();
    if (next) {
        cell = As<Cell>(next);  // advance pointer
    } else {
        cell = {};
    }

    auto [list, proper] = BuildList(cell, false);
    if (!proper) {
        throw RuntimeError{"bad call syntax"};
    }

    return op->Invoke(list, scope);
}
