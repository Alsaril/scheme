#pragma once

#include <memory>
#include <vector>

#include <object.h>
#include <tokenizer.h>
#include <error.h>

bool ValidToken(std::shared_ptr<Object> obj);
void BuildList(std::vector<std::shared_ptr<Object>>& symbols);
std::shared_ptr<Object> Read(Tokenizer* tokenizer);
