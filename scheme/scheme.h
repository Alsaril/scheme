#pragma once

#include <string>
#include <sstream>
#include <optional>
#include <unordered_map>

#include <tokenizer.h>
#include <parser.h>
#include <object.h>
#include <error.h>
#include <runtime.h>
#include <operator.h>
#include <callable.h>
#include <scope.h>

class Interpreter {
    friend class ScopeOperation;

public:
    std::string Eval(const std::string&);

private:
    std::shared_ptr<Scope> scope_ = std::make_shared<Scope>();

    std::shared_ptr<RuntimeCallable> BuildAST(std::shared_ptr<Object> obj, bool);
    std::shared_ptr<RuntimeObject> EvalSymbol(const std::string&, bool, std::shared_ptr<Scope>);
    std::shared_ptr<RuntimeObject> EvalCell(std::shared_ptr<Cell> cell, bool,
                                            std::shared_ptr<Scope>);
    std::pair<std::vector<std::shared_ptr<RuntimeCallable>>, bool> BuildList(std::shared_ptr<Cell>,
                                                                             bool);
};
