#pragma once

#include <string>
#include <vector>
#include <memory>
#include <error.h>

class RuntimeObject {
public:
    virtual ~RuntimeObject() = default;
    virtual std::string ToString() const = 0;
    virtual bool ToBool() const;
};

class Boolean : public RuntimeObject {
public:
    Boolean(bool value) : value_(value) {
    }

    virtual std::string ToString() const override;
    virtual bool ToBool() const override;

    const static std::shared_ptr<Boolean> kTrue;
    const static std::shared_ptr<Boolean> kFalse;

    static std::shared_ptr<Boolean> ValueOf(bool value);

private:
    bool value_;
};

class Int : public RuntimeObject {
public:
    Int(int64_t value) : value_(value) {
    }

    int64_t Value() const;

    virtual std::string ToString() const override;

private:
    int64_t value_;
};

class String : public RuntimeObject {
public:
    String(const std::string& value) : value_(value) {
    }

    virtual std::string ToString() const override;

private:
    std::string value_;
};

class List : public RuntimeObject {
public:
    List(bool proper) : proper_(proper) {
    }

    List(bool proper, std::vector<std::shared_ptr<RuntimeObject>>&& objects)
        : proper_(proper), objects_(std::move(objects)) {
    }

    size_t Size() const {
        return objects_.size();
    }

    bool Proper() const {
        return proper_;
    }

    bool IsPair() const {
        return Size() == 2 && !Proper();
    }

    std::shared_ptr<RuntimeObject> At(size_t index) const {
        return objects_[index];
    }

    std::shared_ptr<RuntimeObject>& At(size_t index) {
        return objects_[index];
    }

    virtual std::string ToString() const override;

private:
    bool proper_;
    std::vector<std::shared_ptr<RuntimeObject>> objects_;
};

template <class T>
std::shared_ptr<T> As(const std::shared_ptr<RuntimeObject>& obj) {
    auto result = std::dynamic_pointer_cast<T>(obj);
    if (!result) {
        throw RuntimeError{"bad type"};
    }
    return result;
}

template <class T>
bool Is(const std::shared_ptr<RuntimeObject>& obj) {
    return dynamic_cast<T*>(obj.get()) != nullptr;
}
