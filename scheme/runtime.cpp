#include <runtime.h>

bool RuntimeObject::ToBool() const {
    return true;
}

std::string Boolean::ToString() const {
    return value_ ? "#t" : "#f";
}

bool Boolean::ToBool() const {
    return value_;
}

const std::shared_ptr<Boolean> Boolean::kTrue = std::make_shared<Boolean>(true);
const std::shared_ptr<Boolean> Boolean::kFalse = std::make_shared<Boolean>(false);

std::shared_ptr<Boolean> Boolean::ValueOf(bool value) {
    return value ? kTrue : kFalse;
}

std::string Int::ToString() const {
    return std::to_string(value_);
}

int64_t Int::Value() const {
    return value_;
}

std::string String::ToString() const {
    return value_;
}

std::string List::ToString() const {
    std::string result;
    result += '(';
    for (size_t i = 0; i < objects_.size(); ++i) {
        if (i > 0) {
            result += ' ';
        }
        if (!proper_ && i + 1 == objects_.size()) {
            result += ". ";
        }
        result += objects_[i]->ToString();
    }
    result += ')';
    return result;
}
